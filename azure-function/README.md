# Azure Function for Forwarding Cloudflare Logs to Elasticsearch

This function will collect Cloudflare logs from a Blob and forward them to an Elasticsearch cluster.

### Testing Locally

To test this function locally, go to _azure-function_ directory and create a _local.settings.json_ file.
Set the variable value acording to environment.

```
{
  "IsEncrypted": false,
  "Values": {
    "FUNCTIONS_WORKER_RUNTIME": "python",
    "AzureWebJobsStorage": "<connection string>",
    "StorageConnection": "<connection string>",
    "elastic_hostname": "<host and port>",
    "elastic_username": "<username>",
    "elastic_password": "<password>"
  }
}
```

Install python modules in a virtualenv.
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Run the command `func start` (Azure Functions Core Tools is required).

### Install

The follow command build and upload the function to Azure (Azure Functions Core Tools is required).

```
cd azure-function
func azure functionapp publish <FUNCTION APP NAME> --build-native-deps --build remote
```

### Configure

##### Configure environment variables

Certain environment variables must be configured so that the function can connect to the Elasticsearch cluster.

| Environment Variable | Description |
| --- | --- |
| elastic_hostname | Address and port of the Elasticsearch endpoint, e.g. '1.2.3.4:9200' |
| elastic_username | The username of the Elasticsearch user |
| elastic_password | The password of the Elasticsearch user |
| StorageConnection | Azure Storage connection strings from storage account where logs are stored |

