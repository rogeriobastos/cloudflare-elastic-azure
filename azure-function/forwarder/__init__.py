from gzip import decompress
from os import getenv
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
import azure.functions as func
#import logging


def connect():
    elastic_hostname = getenv('elastic_hostname')
    elastic_username = getenv('elastic_username')
    elastic_password = getenv('elastic_password')

    return Elasticsearch(
        hosts=[elastic_hostname],
        use_ssl=True,
        verify_certs=False,
        ssl_show_warn=False,
        http_auth=f'{elastic_username}:{elastic_password}'
    )

def gendata(logs):
    for line in logs.splitlines():
        yield line.decode(encoding='utf-8', errors='ignore')

def expand_action(data):
    return '{"create":{}}', data

def main(inputblob: func.InputStream):
#    logging.info(f'Python function triggered by blob {inputblob.name}')
    blob = inputblob.read()
    logs = decompress(blob)
    es = connect()
    r = bulk(
        client=es,
        actions=gendata(logs),
        expand_action_callback=expand_action,
        index='cloudflare',
        pipeline='cloudflare-pipeline-weekly'
    )
