
##### Configure Elasticsearch

Elasticsearch needs to be configured with an ingest pipeline and an index template. You can use the provided script to easily install the required artifacts.

**```./install-artifacts.sh -e https://<REST API ENDPOINT>:<PORT> -u <ELASTIC USER> -p <ELASTIC PASSWORD>```**

Original files is available [here](https://github.com/cloudflare/cloudflare-elastic/tree/master/conf).
