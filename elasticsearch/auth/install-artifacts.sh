#!/bin/bash

function usage {
      echo ""
      echo "usage: install-artifacts.sh -u <username> -p <password> -e <endpoint> -c <cacert>"
      echo "    -u  elasticsearch username"
      echo "    -p  elasticsearch password"
      echo "    -e  elasticsearch endpoint, e.g. https://localhost:9200"
      echo "    -c  cacert file"
      echo ""
}

username=""
password=""
endpoint=""
cacert=""
auth=""

while getopts ":u:p:e:c:h" opt; do
  case $opt in
    u)
      username=$OPTARG
      ;;
    p)
      password=$OPTARG 
      ;;
    e)
      endpoint=$OPTARG 
      ;;
    c)
      cacert="--cacert $OPTARG"
      ;;
    h)
      usage
      exit 1
      ;;
    \?)
      usage
      exit 1
      ;;
  esac
done


if [[ "$username" && "$password" ]];
then
    auth="-u $username:$password"
fi

if [ -z "$endpoint" ];
then
    usage
    exit 1
fi


echo ""
echo "Creating cloudflare_reader role at $endpoint"
curl -X PUT $auth $cacert "$endpoint/_security/role/cloudflare_reader" -H 'Content-Type: application/json' -d @cloudflare_reader_role.json
echo ""
echo "Creating cloudflare_writer role at $endpoint"
curl -X PUT $auth $cacert "$endpoint/_security/role/cloudflare_writer" -H 'Content-Type: application/json' -d @cloudflare_writer_role.json
echo ""
echo "Creating cloudflare_writer user at $endpoint"
curl -X PUT $auth $cacert "$endpoint/_security/user/cloudflare_writer" -H 'Content-Type: application/json' -d @cloudflare_writer_user.json
echo ""
