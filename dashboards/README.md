
##### Kibana Dashboards

Pre-built dashboards for viewing Cloudflare logs and metrics. You must import these into Kibana using the **Import Saved Objects** interface (Management/Kibana/Saved Objects).

Original files is available [here](https://github.com/cloudflare/cloudflare-elastic/tree/master/dashboards).
