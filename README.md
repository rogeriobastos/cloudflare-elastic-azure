# cloudflare-elastic-azure

### Use Elasticsearch and Kibana to visualize Cloudflare logs

# Azure Function for Forwarding Cloudflare Logs to Elasticsearch

This function will collect Cloudflare logs from a Blob bucket and forward them to an Elasticsearch cluster.

This project is based in an AWS lambda function released by Cloudflare, the official documentation can be found on [Cloudflare's](https://developers.cloudflare.com/logs/analytics-integrations/elastic/) site.
